package application;

import java.util.Hashtable;

// Used to store any data related to the day 
public class Day {
	
	// Day of the day
	private String dayDate;
	
	// Number of trips in the day
	private int tripsCount;
	
	// Used to store distinct cars which were used
	private Hashtable<String , String> vehicelsHashTable = new Hashtable<String , String>();
	
	// Hold number of the trips as value picked up from specific location for
	// each taxi type as a key.
	private Hashtable<String , Integer> pickUpFromLocationHashtable = new Hashtable<String , Integer>();
			
	public Day(String date) {
		this.dayDate = date;
		this.tripsCount = 1;
	}
	
	public String getDayDate() {
		return this.dayDate;
	}
	
	public void incTripsCount() {
		this.tripsCount++;
	}
	
	public int getTripsCount() {
		return this.tripsCount;
	}
	
	// Checks if a vehicle was used before and if not add it
	public void addNewVehicel(String vendorId) {
		if(!this.vehicelsHashTable.containsKey(vendorId)) {
			this.vehicelsHashTable.put(vendorId, vendorId);
		}
	}
	
	public int getVehicelsCount() {
		return this.vehicelsHashTable.size();
	}
	
	// Check if the taxi type is used before to increment it or create new one.
	public void incPickedUpFromLocation(String taxiType) {
		if(this.pickUpFromLocationHashtable.containsKey(taxiType)) {
			Integer t = this.pickUpFromLocationHashtable.get(taxiType);
			t++;
			this.pickUpFromLocationHashtable.put(taxiType, t);
		}else {
			this.pickUpFromLocationHashtable.put(taxiType, 1);
		}
	}
	
	public Hashtable<String , Integer> getPickedUpFromLocation(){
		return this.pickUpFromLocationHashtable;
	}
}
