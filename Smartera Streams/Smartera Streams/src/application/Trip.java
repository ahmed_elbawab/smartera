package application;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

// TODO : Cache period of the trip instead of calculating in 100 times
//		  (it will move in the queue from the start till the end in 100 step till it 
//		   is removed from the queue and in each step it will be calculated).

public class Trip {
	
	// Trip data 
	private String taxiType;
	private String vendorId;
	private String pickupDateTime;
	private String dropOffDatetime;
	private String pickupLocationId;
	private String dropOffLocationId;
	private String type;
	
	// Used to calculate the period of the trip.
	private Pattern timePattern = Pattern.compile("\\d{2}:\\d{2}:\\d{2}");
	private Matcher matcher;
	private SimpleDateFormat format = new SimpleDateFormat("HH:mm:ss"); 
	private Date tripStartDate;
	private Date tripEndDate;
	
	// Empty to be used by gson.
	public Trip() {
		
	}

	public String getTaxiType() {
		return taxiType;
	}

	public void setTaxiType(String taxiType) {
		this.taxiType = taxiType;
	}

	public String getVendorId() {
		return vendorId;
	}

	public void setVendorId(String vendorId) {
		this.vendorId = vendorId;
	}

	public String getPickupDateTime() {
		return pickupDateTime;
	}

	public void setPickupDateTime(String pickupDateTime) {
		this.pickupDateTime = pickupDateTime;
	}

	public String getDropOffDatetime() {
		return dropOffDatetime;
	}

	public void setDropOffDatetime(String dropOffDatetime) {
		this.dropOffDatetime = dropOffDatetime;
	}

	public String getPickupLocationId() {
		return pickupLocationId;
	}

	public void setPickupLocationId(String pickupLocationId) {
		this.pickupLocationId = pickupLocationId;
	}

	public String getDropOffLocationId() {
		return dropOffLocationId;
	}

	public void setDropOffLocationId(String dropOffLocationId) {
		this.dropOffLocationId = dropOffLocationId;
	}

	public String getType() {
		return type;
	}

	public void setType(String type) {
		this.type = type;
	}
	
	// Calculate difference between trip start and end and return the value.
	public StringBuilder getTripPeriod() {			
		try {
		    this.tripStartDate = format.parse(this.calculateTime(this.pickupDateTime));
		    this.tripEndDate = format.parse(this.calculateTime(this.dropOffDatetime));
		} catch (ParseException e) {
		    e.printStackTrace();
		} 
		long diff = this.tripEndDate.getTime() - this.tripStartDate.getTime();
		long diffSeconds = diff / 1000 % 60;  
		long diffMinutes = diff / (60 * 1000) % 60; 
		long diffHours = diff / (60 * 60 * 1000);  
		
		StringBuilder tripPeriod = new StringBuilder();
		tripPeriod.append(String.valueOf(diffHours));
		tripPeriod.append(" Hours,");
		tripPeriod.append(String.valueOf(diffMinutes));
		tripPeriod.append(" Minutes,");
		tripPeriod.append(String.valueOf(diffSeconds));
		tripPeriod.append(" Seconds.");
		
		return tripPeriod;
	}
	
	// Extract time from date [2019-02-04 11:05:30]
	private String calculateTime(String time) {
		this.matcher = this.timePattern.matcher(time);
		if(this.matcher.find()) {
			return this.matcher.group();
		}
        return null;
	}
}
