package application;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.Hashtable;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.XYChart;
import javafx.scene.control.Label;
import javafx.stage.Stage;

public class Results {
	
	private String resultsPath = "src/application/results/results";
	private File resultsFile = new File("src/application/results/results");
	
	private static Analysis analysis;
	
	// Holds this controller's Stage
    private final Stage thisStage;
    
    public Results(Analysis analysis) {
    	
    	this.analysis = analysis;

        // Create the new stage
        thisStage = new Stage();

        // Load the FXML file
        try {
            FXMLLoader loader = new FXMLLoader(getClass().getResource("MainGui.fxml"));

            // Set this class as the controller
            loader.setController(this);

            // Load the scene
            thisStage.setScene(new Scene(loader.load()));

            // Setup the window/stage
            thisStage.setTitle("Smartera Streams");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    /**
     * Show the stage that was loaded in the constructor
     */
    public void showStage() {
    	thisStage.show();
    }
	
	public void updateResults() {
		this.updateNumberOfTripsPerDay();
		this.updateAverageVehicelsPerDay();
		this.updateNumberOfNoDropOffLocation();
		this.updatePickedUpFromLocation();
		this.updatePeriodOfEachTrip();
		this.updateFile();
	}
	
	
	  @FXML
	    private BarChart<String, String> tripsPerDay;
	private void updateNumberOfTripsPerDay() {				
		Platform.runLater(new Runnable() {
		      @Override public void run() {
		    	  XYChart.Series dataSeries1 = new XYChart.Series();
		  		
		  		ArrayList<Day> l = analysis.getDaysList();
		  		
		  		for(int i = 0 ; i < l.size() ; i++) {
		  			dataSeries1.getData().add(new XYChart.Data(l.get(i).getDayDate(), l.get(i).getTripsCount()));
		  		}
		    	  tripsPerDay.getData().clear();
		    	  tripsPerDay.getData().add(dataSeries1);
		      }
		});
	}
	
	@FXML
    private Label averageVehiclePerDay;
	private void updateAverageVehicelsPerDay() {
		Platform.runLater(new Runnable() {
		      @Override public void run() {
		    	  averageVehiclePerDay.setText(analysis.getAverageVehicelsPerDay());
		      }
		});
	}
	
	private void updateNumberOfNoDropOffLocation() {
		// TODO : get no drop off location list
	}
	
	private void updatePeriodOfEachTrip() {
		
	}
	
	private void updatePickedUpFromLocation() {
		
	}
	
	public void updateFile() {
		if(!this.resultsFile.exists()){
			this.resultsFile.delete();
		}
		try {
			this.resultsFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		String line = this.analysis.getTotlaNumberOfRecords() + "," + this.analysis.getTotalNumberOfTrips()
					+ "," + this.analysis.getAverageTripsPerDay() + "," + this.analysis.getDistinctVehicelsCount()
					+ "," + this.analysis.getPickedUpFromSecondLocation();
		
		
		try {
		    Files.write(Paths.get(this.resultsPath), (line + "\n").getBytes(), StandardOpenOption.APPEND);
		}catch (IOException e) {
		    //exception handling left as an exercise for the reader
		}
	}
}
