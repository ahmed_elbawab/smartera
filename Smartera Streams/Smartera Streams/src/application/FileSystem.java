package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Hashtable;

// TODO : Add some indexing for file system like breaking history file to a file for 
// 		  each type and store reference for this files in a hash table.
// TODO : Add a cache with a big size(choose by experiment) to be checked first before 
//		  reading the history file on hope to find it so end with no file read operation.
// TODO : Delete the history file on closing the program.
// TODO : Add a hash table as a cache (the location string as key and id as value)
// 		  to store the location which was requested before to make it faster instead 
// 		  of reading the file for the same location especially in our case only 2 
//		  locations are requested.

public class FileSystem {
		
	private String historyFilePath = "src/application/data/history";
	private File historyFile = new File("src/application/data/history");
	
	private File taxiZonesFile = new File("src/application/data/taxi_zones_simple.csv"); 
	private BufferedReader bufferedReader = null;
	private String line;
	private String[] record;
	private String[] location;
	
	// Create new history file and delete old one if exist
	public FileSystem() {
		if(this.historyFile.exists()){
			this.historyFile.delete();
		}
		try {
			this.historyFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	// Check for message in history file and add it if new
	public boolean checkNewMessage(String message) {
		if(!this.readFromFile(message)) {
			return false;
		}
		this.writeToFile(this.historyFilePath, message);
		return true;
	}
	
	// Check if a message is in the history or not
	private boolean readFromFile(String message) {
		try {
			this.bufferedReader = new BufferedReader(new FileReader(this.historyFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		try {
			while ((this.line = this.bufferedReader.readLine()) != null){
				if(this.line.equals(message)) {
					return false;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		try {
			this.bufferedReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	// Write message to history file
	private void writeToFile(String path , String message) {
		try {
		    Files.write(Paths.get(path), (message + "\n").getBytes(), StandardOpenOption.APPEND);
		}catch (IOException e) {
		    //exception handling left as an exercise for the reader
		}
	}
	
	// Read taxi zones file to get location id.
	public int getLocationId(String location) {
		try {
			this.bufferedReader = new BufferedReader(new FileReader(this.taxiZonesFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		try {
			while ((this.line = this.bufferedReader.readLine()) != null){
				if(this.line.contains(location)) {
					this.location = location.split(",");
					this.record = line.split(",");
					if(this.record[1] == this.location[0] && this.record[2] == this.location[1]) {
						break;
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		try {
			this.bufferedReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Integer.parseInt(this.record[0]);
	}
}
