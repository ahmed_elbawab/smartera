package application;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;

import javafx.fxml.FXML;
import javafx.scene.control.Label;

// TODO : Search for which is better in performance , passing parameters to methods 
// 		  or use them from the trip object directly. 
// TODO : Cache the day string instead of calculating it for each use.
// TODO : Cache the day object instead of getting it from the hash table in each use.
// TODO : Check for the hash table .size method implementation to decide if we to use a 
//		  counter if it loops the hash table to get the size.

public class Analysis {
	
	// Used to convert string message into trip object.
	private Gson gson = new Gson();
	private Trip trip;
	private FileSystem fileSystem = new FileSystem();
	
	// Used to extract date from string [2019-07-05 4:09:40].
	private Pattern dayPattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
	private Matcher matcher;
	
	// Stores the days objects as values and the string date as a key. 
	private Hashtable<String, Day> daysHashTable = new Hashtable<String, Day>(); 
	
	// Pattern for the location id. 
	private String noDropOfLocationIdPattern = ".*\\d.*";
	private Hashtable<String , Integer> noDropOffLocationHashTable = new Hashtable<String , Integer>();
	
	// Hash table with each entry a queue of trips for a taxi type.  
	private Hashtable<String , Queue> minutesHashTable = new Hashtable<String , Queue>();
	
	// Used to remove "" in fhv trips.
	private String tripsPickUpLocationIdPattern = "^\"|\"$";
	// Address can be changed as the id is fetched dynamically.
	private String firstPickUpLocation = "Madison,Brooklyn";
	
	private int totalNumberOfRecords = 0;
	private int totalNumberOfTrips = 0;
	
	// Used to change from date string to date object.
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	// Used in checking for number of days between oldest and newest date.
	private Date firstDate;
	private Date lastDate;
	private float daysRangeCount;
	
	// Hold distinct vehicle received overall the system.
	private Hashtable<String , String> distinctVehicelsHashTable = new Hashtable<String , String>();
	
	// Address can be changed as the id is fetched dynamically.
	private String secondPickUpLocation = "Woodside,Queens";
	private int secondPickUpLocationCount = 0;
		
	// Check if a new trip then do analysis steps.
	public void onNewRecord(String message) {
		if(this.checkForNewTrip(message)) {
			this.addNewTrip(this.getDayDate(this.trip.getPickupDateTime()));
			this.addNewVehicel(this.getDayDate(this.trip.getPickupDateTime()) , this.trip.getVendorId());
			this.checkNoDropOffLocation(this.trip.getTaxiType() , this.trip.getDropOffLocationId());
			this.countMinutesPerTrip(this.trip);
			this.checkPickedUpFromLocation(this.getDayDate(this.trip.getPickupDateTime()), 
					this.trip.getTaxiType(), this.trip.getPickupLocationId());
			this.checkDaysRange(this.getDayDate(this.trip.getPickupDateTime()));
			this.checkDistinctVehicels(this.trip.getVendorId());
			this.countPickedUpFromLocation(this.trip.getPickupLocationId());
		}
	}
	
	// Use file system to check if a trip was received before or not.
	private boolean checkForNewTrip(String message) {
		this.totalNumberOfRecords++;
		if(this.fileSystem.checkNewMessage(message)) {
			this.totalNumberOfTrips++;
			this.trip = this.gson.fromJson(message, Trip.class);
			return true;
		}
		return false;
	}
	
	// Extract the date part from the string [2019-09-6 5:30:10].
	private String getDayDate(String stringDate) {
		this.matcher = this.dayPattern.matcher(stringDate);
		if(this.matcher.find()) {
			return this.matcher.group();
		}
        return null;
	}
	
	// Check if the day exists then increment it's trip counter else create new day.
	private void addNewTrip(String day) {
		if(this.daysHashTable.containsKey(day)) {
			this.daysHashTable.get(day).incTripsCount();
		}else {
			this.daysHashTable.put(day, new Day(day));
		}
	}
	
	// Add new vehicle to the day.
	private void addNewVehicel(String day ,String vendorId) {
		this.daysHashTable.get(day).addNewVehicel(vendorId);
	}
	
	// Checks if the trip has no drop off location by using pattern for the location id
	// in case of no drop off then check for taxi type and increment it or create new one.
	private void checkNoDropOffLocation(String taxiType , String dropOffLocationId) {		
		if(!dropOffLocationId.matches(this.noDropOfLocationIdPattern)) {
			if(this.noDropOffLocationHashTable.containsKey(taxiType)) {
				Integer t = this.noDropOffLocationHashTable.get(taxiType);
				t++;
				this.noDropOffLocationHashTable.put(taxiType, t);
			}else {
				this.noDropOffLocationHashTable.put(taxiType, 1);
			}
		}
		if(!this.noDropOffLocationHashTable.containsKey(taxiType)) {
			this.noDropOffLocationHashTable.put(taxiType , 0);
		}
	}
	
	// Check if the taxi type is found to get it's queue and update it or create new one.
	// Queue size is limited to 100 (by me) so pop a trip if it is full. 
	private void countMinutesPerTrip(Trip trip) {
		Queue temp = new LinkedList();
		if(this.minutesHashTable.containsKey(trip.getTaxiType())) {
			temp = this.minutesHashTable.get(trip.getTaxiType());
			if(temp.size() >= 100) {
				temp.remove();
			}
		}
		temp.add(trip);
		this.minutesHashTable.put(trip.getTaxiType(), temp);
	}
	
	// Check for trips picked up from the passed address by first getting the location id
	// then check the trip pick up location id and if match add it to the day.
	private void checkPickedUpFromLocation(String date , String taxiType , String locationId) {
		// Remove "" surrounding location id in fhv trips.
		locationId = locationId.replaceAll(this.tripsPickUpLocationIdPattern, "");
		if(!locationId.isEmpty() && 
				Integer.parseInt(locationId) == this.fileSystem.getLocationId(this.firstPickUpLocation)) {
			this.daysHashTable.get(date).incPickedUpFromLocation(taxiType);
		}
	}
	
	// Check for oldest and newest day received and get number in days in between to use it 
	// in calculating the range.
	private void checkDaysRange(String tripDate) {
		try {
			Date temp = dateFormat.parse(tripDate);
			if(this.firstDate == null) {
				this.firstDate = temp;
			}
			if(this.lastDate == null) {
				this.lastDate = temp;
			}
			if(temp.before(this.firstDate)) {
				this.firstDate = temp;
			}
			if(temp.after(this.lastDate)) {
				this.lastDate = temp;
			}
		    long diff = this.lastDate.getTime() - this.firstDate.getTime();
		    this.daysRangeCount = (diff / (1000*60*60*24));
		    this.daysRangeCount++;
		} catch (ParseException e) {
		    e.printStackTrace();
		}
	}
	
	// Check if a vehicle was received before or add it.
	private void checkDistinctVehicels(String vendorId) {
		if(!this.distinctVehicelsHashTable.containsKey(vendorId)) {
			this.distinctVehicelsHashTable.put(vendorId, vendorId);
		}
	}
	
	// Check for pick up location and increment on match.
	private void countPickedUpFromLocation(String locationId) {
		// Remove "" in fhv trips.
		locationId = locationId.replaceAll(this.tripsPickUpLocationIdPattern, "");
		if(!locationId.isEmpty() && 
				Integer.parseInt(locationId) == this.fileSystem.getLocationId(this.secondPickUpLocation)) {
			this.secondPickUpLocationCount++;
		}
	}
	
//	public void printHashTable() {
//		System.out.println("Display number of trips per day:"); 
//		ArrayList<Day> l = new ArrayList<>(this.daysHashTable.values());
//        for (int i = 0 ; i < l.size() ; i++) {
//        	System.out.println(l.get(i).getDayDate() + " : " + l.get(i).getTripsCount());
//        }
//	}
	
	public ArrayList<Day> getDaysList(){
		return new ArrayList<>(this.daysHashTable.values());
	}
	
//	public void printAverageVehicelsPerDay() {
//		System.out.println("Display average number of vehicels per day:"); 
//		ArrayList<Day> l = new ArrayList<>(this.daysHashTable.values());
//		int vehicelsCount = 0;
//        for (int i = 0 ; i < l.size() ; i++) {
//        	vehicelsCount  = vehicelsCount + l.get(i).getVehicelsCount();
//        }
//		System.out.println(vehicelsCount + " Vehicels" + "/ " + this.daysHashTable.size()
//		+ " Days" + " = "  + ((vehicelsCount/(float)this.daysHashTable.size())));
//	}
	
	public String getAverageVehicelsPerDay() {
		ArrayList<Day> l = new ArrayList<>(this.daysHashTable.values());
		int vehicelsCount = 0;
        for (int i = 0 ; i < l.size() ; i++) {
        	vehicelsCount  = vehicelsCount + l.get(i).getVehicelsCount();
        }
		return (vehicelsCount + " Vehicels" + "/ " + this.daysHashTable.size()
		+ " Days" + " = "  + ((vehicelsCount/(float)this.daysHashTable.size())));
	}
	
//	public void printNoDropLocation() {	
//		System.out.println("Display number of no drop of location trips for each type:"); 
//		for (String key : this.noDropOffLocationHashTable.keySet()) {
//		    System.out.println(key + ":" + this.noDropOffLocationHashTable.get(key));
//		}
//	}
	
	public Hashtable<String, Integer> getNoDropOffLocationList() {
		return this.noDropOffLocationHashTable;
	}
	
//	public void printMinsPerEachTrip() {
//		System.out.println("Display minutes per trip for each taxi type:"); 
//		ArrayList<Queue<Trip>> l = new ArrayList<>(this.minutesHashTable.values());
//		for (int i = 0 ; i < l.size() ; i++) {
//			for (Trip t : l.get(i)) {
//				System.out.println(t.getTaxiType() + " " + t.getTripPeriod());
//			}
//        }
//	}
	
	public ArrayList<Queue<Trip>> getMinsPerEachTrip() {
		return new ArrayList<>(this.minutesHashTable.values());
	}
	
//	public void printPickedUpFromLocation() {
//		System.out.println("Display number of picked up from location per each day:"); 
//		ArrayList<Day> l = new ArrayList<>(this.daysHashTable.values());
//        for (int i = 0 ; i < l.size() ; i++) {
//        	System.out.println(l.get(i).getDayDate() + " : ");
//        	for (String key : l.get(i).getPickedUpFromLocation().keySet()) {
//    		    System.out.println("   " +  key + ":" + l.get(i).getPickedUpFromLocation().get(key));
//    		}
//        }
//	}
	
	public ArrayList<Day> getPickedUpFromLocation() {
		return new ArrayList<>(this.daysHashTable.values());
	}
	
//	public void printTotlaNumberOfRecords() {
//		System.out.println("Display total number of records: " + this.totalNumberOfRecords); 
//	}
	
	public int getTotlaNumberOfRecords() {
		return this.totalNumberOfRecords;
	}
	
//	public void printTotalNumberOfTrips() {
//		System.out.println("Display total number of trips: " + this.totalNumberOfTrips); 
//	}
	
	public int getTotalNumberOfTrips() {
		return this.totalNumberOfTrips;
	}
	
//	public void printAverageTripsPerDay() {
//		System.out.println("Display average trips per day: "
//				+ this.totalNumberOfTrips + " Trips /" + this.daysRangeCount + " Days"
//				+ " = " + this.totalNumberOfTrips / this.daysRangeCount); 
//	}
	
	public String getAverageTripsPerDay() {
		return ("Display average trips per day: "
				+ this.totalNumberOfTrips + " Trips /" + this.daysRangeCount + " Days"
				+ " = " + this.totalNumberOfTrips / this.daysRangeCount);
	}
	
//	public void printDistinctVehicelsCount() {
//		System.out.println("Display number of distinct vehicels: " + this.distinctVehicelsHashTable.size()); 
//	}
	
	public int getDistinctVehicelsCount() {
		return this.distinctVehicelsHashTable.size(); 
	}

//	public void printPickedUpFromSecondLocation() {
//		System.out.println("Display number of picked from second location: " + this.secondPickUpLocationCount); 
//	}
	
	public int getPickedUpFromSecondLocation() {
		return this.secondPickUpLocationCount;
	}
}
