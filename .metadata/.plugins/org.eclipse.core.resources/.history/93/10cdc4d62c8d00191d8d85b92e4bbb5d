import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;

public class Analysis {
	
	private Gson gson = new Gson();
	private Trip trip;
	private FileSystem fileSystem = new FileSystem();
	
	private Pattern dayPattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
	private Matcher matcher;
	
	private Hashtable<String, Day> daysHashTable = new Hashtable<String, Day>(); 
	
	private String noDropOfLocationIdPattern = ".*\\d.*";
	private Hashtable<String , Integer> noDropOffLocationHashTable = new Hashtable<String , Integer>();
	
	private Hashtable<String , Queue> minutesHashTable = new Hashtable<String , Queue>();
	
	private String tripsPickUpLocationIdPattern = "^\"|\"$";
	private String firstPickUpLocation = "Madison,Brooklyn";
	
	private int totalNumberOfRecords = 0;
	private int totalNumberOfTrips = 0;
	
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private Date firstDate;
	private Date lastDate;
	private float daysRangeCount;
	
	private Hashtable<String , String> distinctVehicelsHashTable = new Hashtable<String , String>();
	
	private String secondPickUpLocation = "Woodside,Queens";
	private int secondPickUpLocationCount = 0;
		
	public void onNewRecord(String message) {
		if(this.checkForNewTrip(message)) {
			this.addNewTrip(this.getDayDate(this.trip.getPickupDateTime()));
			this.addNewVehicel(this.getDayDate(this.trip.getPickupDateTime()) , this.trip.getVendorId());
			this.checkNoDropOffLocation(this.trip.getTaxiType() , this.trip.getDropOffLocationId());
			this.countMinutesPerTrip(this.trip);
			this.checkPickedUpFromLocation(this.getDayDate(this.trip.getPickupDateTime()), 
					this.trip.getTaxiType(), this.trip.getPickupLocationId());
			this.checkDaysRange(this.getDayDate(this.trip.getPickupDateTime()));
			this.checkDistinctVehicels(this.trip.getVendorId());
			this.countPickedUpFromLocation(this.trip.getPickupLocationId());
		}
	}
	
	private boolean checkForNewTrip(String message) {
		this.totalNumberOfRecords++;
		if(this.fileSystem.checkNewMessage(message)) {
			this.totalNumberOfTrips++;
			this.trip = this.gson.fromJson(message, Trip.class);
			return true;
		}
		return false;
	}
	
	private String getDayDate(String stringDate) {
		this.matcher = this.dayPattern.matcher(stringDate);
		if(this.matcher.find()) {
			return this.matcher.group();
		}
        return null;
	}
	
	private void addNewTrip(String day) {
		if(this.daysHashTable.containsKey(day)) {
			this.daysHashTable.get(day).incTripsCount();
		}else {
			this.daysHashTable.put(day, new Day(day));
		}
	}
	
	private void addNewVehicel(String day ,String vendorId) {
		this.daysHashTable.get(day).addNewVehicel(vendorId);
	}
	
	private void checkNoDropOffLocation(String taxiType , String dropOffLocationId) {		
		if(!dropOffLocationId.matches(this.noDropOfLocationIdPattern)) {
			if(this.noDropOffLocationHashTable.containsKey(taxiType)) {
				Integer t = this.noDropOffLocationHashTable.get(taxiType);
				t++;
				this.noDropOffLocationHashTable.put(taxiType, t);
			}else {
				this.noDropOffLocationHashTable.put(taxiType, 1);
			}
		}
		if(!this.noDropOffLocationHashTable.containsKey(taxiType)) {
			this.noDropOffLocationHashTable.put(taxiType , 0);
		}
	}
	
	private void countMinutesPerTrip(Trip trip) {
		Queue temp = new LinkedList();
		if(this.minutesHashTable.containsKey(trip.getTaxiType())) {
			temp = this.minutesHashTable.get(trip.getTaxiType());
			if(temp.size() >= 100) {
				temp.remove();
			}
		}
		temp.add(trip);
		this.minutesHashTable.put(trip.getTaxiType(), temp);
	}
	
	private void checkPickedUpFromLocation(String date , String taxiType , String locationId) {
		locationId = locationId.replaceAll(this.tripsPickUpLocationIdPattern, "");
		if(!locationId.isEmpty() && 
				Integer.parseInt(locationId) == this.fileSystem.getLocationId(this.firstPickUpLocation)) {
			this.daysHashTable.get(date).incPickedUpFromLocation(taxiType);
		}
	}
	
	private void checkDaysRange(String tripDate) {
		try {
			Date temp = dateFormat.parse(tripDate);
			if(this.firstDate == null) {
				this.firstDate = temp;
			}
			if(this.lastDate == null) {
				this.lastDate = temp;
			}
			if(temp.before(this.firstDate)) {
				this.firstDate = temp;
			}
			if(temp.after(this.lastDate)) {
				this.lastDate = temp;
			}
		    long diff = this.lastDate.getTime() - this.firstDate.getTime();
		    this.daysRangeCount = (diff / (1000*60*60*24));
		    this.daysRangeCount++;
		} catch (ParseException e) {
		    e.printStackTrace();
		}
	}
	
	private void checkDistinctVehicels(String vendorId) {
		if(!this.distinctVehicelsHashTable.containsKey(vendorId)) {
			this.distinctVehicelsHashTable.put(vendorId, vendorId);
		}
	}
	
	private void countPickedUpFromLocation(String locationId) {
		locationId = locationId.replaceAll(this.tripsPickUpLocationIdPattern, "");
		if(!locationId.isEmpty() && 
				Integer.parseInt(locationId) == this.fileSystem.getLocationId(this.secondPickUpLocation)) {
			this.secondPickUpLocationCount++;
		}
	}
	
	public void printHashTable() {
		System.out.println("Display number of trips per day:"); 
		ArrayList<Day> l = new ArrayList<>(this.daysHashTable.values());
        for (int i = 0 ; i < l.size() ; i++) {
        	System.out.println(l.get(i).getDayDate() + " : " + l.get(i).getTripsCount());
        }
	}
	
	public void printAverageVehicelsPerDay() {
		System.out.println("Display average number of vehicels per day:"); 
		ArrayList<Day> l = new ArrayList<>(this.daysHashTable.values());
		int vehicelsCount = 0;
        for (int i = 0 ; i < l.size() ; i++) {
        	vehicelsCount  = vehicelsCount + l.get(i).getVehicelsCount();
        }
		System.out.println(vehicelsCount + " Vehicels" + "/ " + this.daysHashTable.size()
		+ " Days" + " = "  + ((vehicelsCount/(float)this.daysHashTable.size())));
	}
	
	public void printNoDropLocation() {	
		System.out.println("Display number of no drop of location trips for each type:"); 
		for (String key : this.noDropOffLocationHashTable.keySet()) {
		    System.out.println(key + ":" + this.noDropOffLocationHashTable.get(key));
		}
	}
	
	public void printMinsPerEachTrip() {
		System.out.println("Display minutes per trip for each taxi type:"); 
		ArrayList<Queue<Trip>> l = new ArrayList<>(this.minutesHashTable.values());
		for (int i = 0 ; i < l.size() ; i++) {
			for (Trip t : l.get(i)) {
				System.out.println(t.getTaxiType() + " " + t.getTripPeriod());
			}
        }
	}
	
	public void printPickedUpFromLocation() {
		ArrayList<Day> l = new ArrayList<>(this.daysHashTable.values());
        System.out.println("display number of picked up from location per each day:"); 
        for (int i = 0 ; i < l.size() ; i++) {
        	System.out.println(l.get(i).getDayDate() + " : ");
        	for (String key : l.get(i).getPickedUpFromLocation().keySet()) {
    		    System.out.println("   " +  key + ":" + l.get(i).getPickedUpFromLocation().get(key));
    		}
        }
	}
	
	public void printTotlaNumberOfRecords() {
		System.out.println("display total number of records: " + this.totalNumberOfRecords); 
	}
	
	public void printTotalNumberOfTrips() {
		System.out.println("display total number of trips: " + this.totalNumberOfTrips); 
	}
	
	public void printAverageTripsPerDay() {
		System.out.println("display average trips per day: "
				+ this.fileSystem.getTripsCount() + " Trips /" + this.daysRangeCount + " Days"
				+ " = " + this.fileSystem.getTripsCount() / this.daysRangeCount); 
	}
	
	public void printDistinctVehicelsCount() {
		System.out.println("display number of distinct vehicels: " + this.distinctVehicelsHashTable.size()); 
	}
}
