import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Queue;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.google.gson.Gson;

public class Analysis {
	
	private Gson gson = new Gson();
	private Trip trip;
	private FileSystem fileSystem = new FileSystem();
	
	private Hashtable<String, Day> daysHashTable = new Hashtable<String, Day>(); 
	private Pattern dayPattern = Pattern.compile("\\d{4}-\\d{2}-\\d{2}");
	private Matcher matcher;
	
	private Hashtable<String , Integer> noDropLocationHashTable = new Hashtable<String , Integer>();
	private String noDropOfLocationIdPattern = ".*\\d.*";
	
	private Hashtable<String , Queue> minsHashTable = new Hashtable<String , Queue>();
	
	private String tripsPickedUpFromLocation = "Madison,Brooklyn";
	private int tripsPickedUpFromLocationId = 149;
	private String tripsPickedUpLocationIdPattern = "^\"|\"$";
	
	private int totalNumberOfRecords = 0;
	
	// TODO : average trips per day for the given month
	SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");
	private Date firstDate;
	private Date lastDate;
	private float daysRange;
	
	private Hashtable<String , String> distinctVehicelsHashTable = new Hashtable<String , String>();
	
	// TODO : total number of trips picked up from “Woodside,Queens” for all three taxi types.
	
	public void onNewRecord(String message) {
		if(this.checkForNewTrip(message)) {
			this.incrementTrips(this.getDayDate(this.trip.getPickupDateTime()));
			this.countVehicels(this.getDayDate(this.trip.getPickupDateTime()) , this.trip.getVendorId());
			this.countNoDropLocation(this.trip.getTaxiType() , this.trip.getDropOffLocationId());
			this.countMinsPerEachTrip();
			this.checkPickedUpFromLocation(this.getDayDate(this.trip.getPickupDateTime()), 
					this.trip.getTaxiType(), this.trip.getPickupLocationId());
			this.checkDaysRange(this.getDayDate(this.trip.getPickupDateTime()));
			// TODO : number of distinct vehicles
			// TODO : total number of trips picked up from “Woodside,Queens” for all three taxi types.
		}
	}
	
	private void checkDaysRange(String tripDate) {
		try {
			Date temp = myFormat.parse(tripDate);
			
			if(this.firstDate == null) {
				this.firstDate = temp;
			}
			
			if(this.lastDate == null) {
				this.lastDate = temp;
			}
			
			if(temp.before(this.firstDate)) {
				this.firstDate = temp;
			}
			
			if(temp.after(this.lastDate)) {
				this.lastDate = temp;
			}

		    long diff = this.lastDate.getTime() - this.firstDate.getTime();
		    
		    this.daysRange = (diff / (1000*60*60*24));
		    
		} catch (ParseException e) {
		    e.printStackTrace();
		}
	}
	
	private boolean checkForNewTrip(String message) {
		this.totalNumberOfRecords++;
		if(this.fileSystem.checkNewMessage(message)) {
			this.trip = this.gson.fromJson(message, Trip.class);
			return true;
		}
		return false;
	}
	
	private void incrementTrips(String day) {
		if(this.daysHashTable.containsKey(day)) {
			this.daysHashTable.get(day).incTripsCount();
		}else {
			this.daysHashTable.put(day, new Day(day));
		}
	}
	
	private String getDayDate(String stringDate) {
		this.matcher = this.dayPattern.matcher(stringDate);
		if(this.matcher.find()) {
			return this.matcher.group();
		}
        return null;
	}
	
	private void countVehicels(String day ,String vendorId) {
		this.daysHashTable.get(day).addNewVehicels(vendorId);
	}
	
	private void countNoDropLocation(String taxiType , String dropOffLocationId) {		
		if(!dropOffLocationId.matches(this.noDropOfLocationIdPattern)) {
			if(this.noDropLocationHashTable.containsKey(taxiType)) {
				Integer t = this.noDropLocationHashTable.get(taxiType);
				t++;
				this.noDropLocationHashTable.put(taxiType, t);
			}else {
				this.noDropLocationHashTable.put(taxiType, 1);
			}
		}
		if(!this.noDropLocationHashTable.containsKey(taxiType)) {
			this.noDropLocationHashTable.put(taxiType , 0);
		}
	}
	
	private void countMinsPerEachTrip() {
		Queue temp = new LinkedList();
		if(this.minsHashTable.containsKey(this.trip.getTaxiType())) {
			temp = this.minsHashTable.get(this.trip.getTaxiType());
			if(temp.size() >= 100) {
				temp.remove();
			}
		}
		temp.add(this.trip);
		this.minsHashTable.put(this.trip.getTaxiType(), temp);
	}
	
	private void checkPickedUpFromLocation(String date , String taxiType , String location) {
		location = location.replaceAll(this.tripsPickedUpLocationIdPattern, "");
		if(!location.isEmpty() && Integer.parseInt(location) == this.getLocationId(tripsPickedUpFromLocation)) {
			this.daysHashTable.get(date).incPickedUpFromLocation(taxiType);
		}
	}
	
	private int getLocationId(String location) {
		// TODO : read file to get location id
		if(location == this.tripsPickedUpFromLocation) {
			return this.tripsPickedUpFromLocationId;
		}
		return 0;
	}
	
	public void printHashTable() {
		ArrayList<Day> l = new ArrayList<>(this.daysHashTable.values());
        System.out.println("display number of trips per day:"); 
        for (int i = 0 ; i < l.size() ; i++) {
        	System.out.println(l.get(i).getDayDate() + " : " + l.get(i).getTripsCount());
        }
	}
	
	public void printNoDropLocation() {	
		System.out.println("display number of no drop of location trips for each type:"); 
		for (String key : this.noDropLocationHashTable.keySet()) {
		    System.out.println(key + ":" + this.noDropLocationHashTable.get(key));
		}
	}
	
	public void printAverageVehicelsPerDay() {
		System.out.println("display average number of vehicels per day:"); 
		ArrayList<Day> l = new ArrayList<>(this.daysHashTable.values());
		int vehicelsCount = 0;
        for (int i = 0 ; i < l.size() ; i++) {
        	vehicelsCount  = vehicelsCount + l.get(i).getVehicelsCount();
        }
		System.out.println(vehicelsCount + " Vehicels" + "/ " + this.daysHashTable.size()
		+ " Days" + " = "  + ((vehicelsCount/(float)this.daysHashTable.size())));
	}
	
	public void printCountMinsPerEachTrip() {
		// TODO : print number of minutes
		System.out.println("display minutes per trip for each taxi type:"); 
		ArrayList<Queue<Trip>> l = new ArrayList<>(this.minsHashTable.values());
		for (int i = 0 ; i < l.size() ; i++) {
			for (Trip t : l.get(i)) {
				System.out.println(t.getTaxiType() + " " + t.getTripPeriod());
			}
        }
	}
	
	public void printPickedUpFromLocation() {
		ArrayList<Day> l = new ArrayList<>(this.daysHashTable.values());
        System.out.println("display number of picked up from location per each day:"); 
        for (int i = 0 ; i < l.size() ; i++) {
        	System.out.println(l.get(i).getDayDate() + " : ");
        	for (String key : l.get(i).getPickedUpFromLocation().keySet()) {
    		    System.out.println("   " +  key + ":" + l.get(i).getPickedUpFromLocation().get(key));
    		}
        }
	}
	
	public void printTotlaNumberOfRecords() {
		System.out.println("display total number of records: " + this.totalNumberOfRecords); 
	}
	
	public void printTotalNumberOfTrips() {
		System.out.println("display total number of trips: " + this.fileSystem.getTripsCount()); 
	}
	
	public void printAverageTripsPerDay() {
		System.out.println(this.firstDate);
		System.out.println(this.lastDate);
	}
}
