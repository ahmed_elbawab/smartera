import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Hashtable;

public class FileSystem {
	
	private Hashtable<String, String> tripsHashTable = new Hashtable<String, String>(); 
	
	private String historyFilePath = "src/data/history";
	private File historyFile = new File("src/data/history");
	
	private File taxiZonesFile = new File("src/data/taxi_zones_simple.csv"); 
	private BufferedReader bufferedReader = null;
	private String line;
	private String[] record;
	private String[] location;
	
	public FileSystem() {
		if(this.historyFile.exists()){
			this.historyFile.delete();
			try {
				this.historyFile.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

	public boolean checkNewMessage(String message) {
		if(!this.readFromFile(message)) {
			return false;
		}
		this.writeToFile(this.historyFilePath, message);
		return true;
	}
	
	private boolean readFromFile(String message) {
		try {
			this.bufferedReader = new BufferedReader(new FileReader(this.historyFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		try {
			while ((this.line = this.bufferedReader.readLine()) != null){
				if(this.line.equals(message)) {
					return false;
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		try {
			this.bufferedReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return true;
	}
	
	private void writeToFile(String path , String message) {
		try {
		    Files.write(Paths.get(path), (message + "\n").getBytes(), StandardOpenOption.APPEND);
		}catch (IOException e) {
		    //exception handling left as an exercise for the reader
		}
	}
	
	public int getLocationId(String location) {
		try {
			this.bufferedReader = new BufferedReader(new FileReader(this.taxiZonesFile));
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}   
		try {
			while ((this.line = this.bufferedReader.readLine()) != null){
				if(this.line.contains(location)) {
					this.location = location.split(",");
					this.record = line.split(",");
					if(this.record[1] == this.location[0] && this.record[2] == this.location[1]) {
						break;
					}
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		try {
			this.bufferedReader.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return Integer.parseInt(this.record[0]);
	}
}
