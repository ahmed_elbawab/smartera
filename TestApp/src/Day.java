import java.util.Hashtable;

public class Day {
	
	private String dayDate;
	
	private int tripsCount;
	
	private Hashtable<String , Integer> pickUpFromLocationHashtable = new Hashtable<String , Integer>();
	
	private Hashtable<String , String> vehicelsHashTable = new Hashtable<String , String>();
		
	public Day(String date) {
		this.dayDate = date;
		this.tripsCount = 1;
	}
	
	public String getDayDate() {
		return this.dayDate;
	}
	
	public void incTripsCount() {
		this.tripsCount++;
	}
	
	public int getTripsCount() {
		return this.tripsCount;
	}
	
	public void addNewVehicel(String vendorId) {
		if(!this.vehicelsHashTable.containsKey(vendorId)) {
			this.vehicelsHashTable.put(vendorId, vendorId);
		}
	}
	
	public int getVehicelsCount() {
		return this.vehicelsHashTable.size();
	}
	
	public void incPickedUpFromLocation(String taxiType) {
		if(this.pickUpFromLocationHashtable.containsKey(taxiType)) {
			Integer t = this.pickUpFromLocationHashtable.get(taxiType);
			t++;
			this.pickUpFromLocationHashtable.put(taxiType, t);
		}else {
			this.pickUpFromLocationHashtable.put(taxiType, 1);
		}
	}
	
	public Hashtable<String , Integer> getPickedUpFromLocation(){
		return this.pickUpFromLocationHashtable;
	}
}
