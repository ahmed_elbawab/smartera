Smartera Streams:
-----------------


1) Notes:
---------
- In the git you can find 2 folders (testapp) and (Smartera streams).
- I worked on the (testapp) to finish all the structure, implementation and main functionality.
- I added the GUI and writing results in files in the (Smartera streams).
- The app can work on any data, any number of taxi types , any location and not build upon a specific assumption.
- There is a bug on the first run of the server and the app which is solved by running the app again (will be mentioned below).


2) How to run:
--------------
- Download the project from git.
- Open the project in any editor.
- Run the server.
- For (testapp) you should write the period you want the app to run (ex: 3 so you will get results for listening to the server for 3 seconds) and the results will be printed in the consol.
- For (Smartera Streams) Run the project to open the GUI screen and notice the changing of values in the board and in the files.


3) Description:
---------------
- The app starts by listening to the serving and writing messages in a buffer queue.
- A task run every second to fetch the queue element by element and do analysis step by step then finally update the UI.
- File system checks if the message was sent before by reading a history file.
- If the message is new it is converted to a trip object and analysis start.
- I mainly used hash tables to make it O(1) search and to make the size dynamic to accept any data.
- I created a day object to for each day and store in it the trips count, vehicles and no drop off location.
- After finishing analysis all the queue the GUI is updated.


4) Bugs and Solutions:
----------------------
- On starting the API and the app for first time the app didn’t get any values and show null 
- I tried to solve this by sending a message to the API but the API prints requests will be denied so to be safe I stopped sending messages.
- You can get over the bug by running the app again.
- I suggest to call connectToApi() twice but actually I didn’t try it.


5) Requirements:
----------------
- All the requirements are done and functionally working.
- Writing to files is done.
- Dashboard isn’t complete but the rest work is easy and I did them before:
         * No drop off location chart and I made the trips per day chart
         * Two tables (can be made as bar charts) and I already used tables in           my faculty projects (Bookstore Database project).
- I’m very sorry for not completing them I know the competition was announced a long time ago but I had a lot of projects and exams at my faculty and currently working on final exams ^_^.


6) What Next:
-------------
- Complete the GUI.
- Improve the file system by adding some indexing on the history file like breaking it into multiple of file (ex: file for each taxi type) which will make the search process faster.
- Apply caching concept on some data which is retrieved more than one time (ex: location id instead of searching for each request)
- Fix the on start bug.
- Try to use multithreading to make the app faster in analysis process.
- Apply write back concept to avoid opening and closing the file with each trip by clustering the writes.


Finally, 
Thanks for this great task as I have learned a lot from it ^_^.

